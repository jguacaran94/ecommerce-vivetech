class AddImportation < ActiveRecord::Migration[6.1]
  def up
    create_table :importations do |t|
      t.integer :total_success, default: 0
      t.integer :total_error, default: 0
      t.integer :total_products, default: 0

      t.timestamps
    end
  end
  def down
    drop_table :importations
  end
end
