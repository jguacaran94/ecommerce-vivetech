class AddProduct < ActiveRecord::Migration[6.1]
  def up
    create_table :products do |t|
      t.string :name, null: false
      t.string :description, null: false
      t.string :importation_id

      t.timestamps
    end
  end
  def down
    drop_table :products
  end
end
