class AddVariant < ActiveRecord::Migration[6.1]
  def up
    create_table :variants do |t|
      t.string :name, null: false
      t.float :price, null: false
      t.integer :product_id, null: false
      t.index ['product_id'], name: 'index_variants_on_product_id'

      t.timestamps
    end
  end
  def down
    drop_table :variants
  end
end
