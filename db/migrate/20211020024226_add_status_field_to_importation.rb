class AddStatusFieldToImportation < ActiveRecord::Migration[6.1]
  def up
    add_column :importations, :status, :integer, default: 0
  end
  def down
    remove_column :importations, :status
  end
end
