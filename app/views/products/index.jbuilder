json.array! @products do |p|
  json.name p.name
  json.description p.description
end