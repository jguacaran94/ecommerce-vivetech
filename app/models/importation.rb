class Importation < ApplicationRecord
  has_many :products

  enum status: %i[pending processing complete]
end