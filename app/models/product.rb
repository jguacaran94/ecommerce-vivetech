class Product < ApplicationRecord
  belongs_to :importation
  has_many :variants, inverse_of: :product

  accepts_nested_attributes_for :variants

  validates :name, :description, presence: true

  before_validation :check_invalid_variants
  
  private
  def check_invalid_variants
    return if variants.all?(&:valid?)
    return if variants.all?(&:invalid?)

    self.variants = variants.select(&:valid?)
  end
end