class Variant < ApplicationRecord
  belongs_to :product, inverse_of: :variants

  validates :name, :price, :product, presence: true
  validates :price, numericality: { greater_than: 0 }
end