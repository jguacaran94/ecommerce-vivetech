class ProductsImportationJob < ApplicationJob
  queue_as :default

  def perform(products, importation_id)
    importation = Importation.find(importation_id)
    ApplicationRecord.transaction do
      importation.update(total_products: products.size, status: :processing)
      products.each do |product_params|
        product = Product.new(product_params.merge(importation_id: importation_id))
        if product.save
          importation.update(total_success: importation.total_success + 1)
        else
          importation.update(total_error: importation.total_error + 1)
        end
      end
      importation.update(status: :complete)
    end
  end
end
