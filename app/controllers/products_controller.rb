class ProductsController < InheritedResources::Base
  skip_before_action :verify_authenticity_token
  
  def index
    @products = Product.all
    respond_to do |f|
      f.html
      f.json { render json: @products }
    end
  end

  def create
    products = permitted_params[:products]
    importation = Importation.create
    ProductsImportationJob.perform_later(products.take(10000), importation.id)
    message = 'The importation request is being processed.'
    message = 'The importation request is being processed. Since the request is greater than 10,000 products, the first 10,000 will be taken.' if products.length > 10000
    respond_to do |f|
      f.html
      f.json {
        render json: {
          message: message
        }
      }
    end
  end
  
  private
  def permitted_params
    fields = %i[name description]
    fields.push(variants_attributes: %i[name price])
    params.permit(products: fields)
  end
end