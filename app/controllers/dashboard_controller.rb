class DashboardController < InheritedResources::Base
  before_action :get_totals, only: %i[index]
  def index
    @importations = Importation.all
  end

  private
  def get_totals
    @created_products = Importation.sum(:total_success)
    @rejected_products = Importation.sum(:total_error)
    @requested_products = Importation.sum(:total_products)
  end
end
