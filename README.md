# To configure this application on your workstation, you will need to do:

#### Install GPG keys:
```
$ gpg --keyserver hkp://pool.sks-keyservers.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
```

#### Install RVM stable:
```
$ \curl -sSL https://get.rvm.io | bash -s stable
```

#### Add this lines at end on your ~/.bash_profile:
```
# Set your Rails environment to development
RAILS_ENV=development

# Add RVM to PATH for scripting.
# Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

# Load RVM into a shell session *as a function*
# RVM bash completion
[[ -s "$HOME/.rvm/scripts/completion" ]] && . "$HOME/.rvm/scripts/completion"
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

# Trigger rvm hooks to autoswitch ruby versions. RUN AS LAST SENTENCE!!!
cd .
```

#### Reset your terminal session:
```
$ reset
```

#### Install Ruby 3.0.0:
```
$ rvm install 3.0.0
```

#### Then, install Git:
- [Install Git on GNU/Linux](https://git-scm.com/download/linux)
- [Install Git on Windows](https://git-scm.com/download/win)
- [Install Git on macOS](https://git-scm.com/download/mac)

#### Clone repo:
```
$ git clone git@gitlab.com:jguacaran94/ecommerce-vivetech.git
```

#### Change to application directory:
```
$ cd ecommerce-vivetech/
```

#### Install Bundler gem:
```
$ gem install bundler
```

#### Install application dependencies:
```
$ bundle install
```

#### Edit config/database.yml configuration with your postgresql development credentials, taking this example:
```
default: &default
  adapter: postgresql
  encoding: unicode
  pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>
  username: postgres
  password: postgres
  host: localhost
  port: 5432
development:
  <<: *default
  database: ecommerce_vivetech_test_development
test:
  <<: *default
  database: ecommerce_vivetech_test_test
production:
  <<: *default
  host: <%= Rails.application.credentials.dig(:database, :host) %>
  database: <%= Rails.application.credentials.dig(:database, :name) %>
  username: <%= Rails.application.credentials.dig(:database, :user) %>
  password: <%= Rails.application.credentials.dig(:database, :password) %>
```

#### Run database creation:
```
$ RAILS_ENV=development bundle exec rails db:create
```

#### Run database migrations:
```
$ RAILS_ENV=development bundle exec rails db:migrate
```

#### Then, you can start the application server with:
```
$ RAILS_ENV=development bundle exec rails s -b 0.0.0.0 -p 3000
```

#### Now, open this link on your browser:
- [http://localhost:3000](http://localhost:3000/)

# To deploy this application on Heroku from your workstation:

#### First, follow this tutorial link:
- [Install the Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli#download-and-install)

#### Next, you must create an Heroku account and login on Heroku CLI:
```
$ heroku login
```

#### Create a Heroku app:
```
$ heroku apps:create
```

#### Now set your database configuration following this link:
- [Connecting in Ruby](https://devcenter.heroku.com/articles/heroku-postgresql#connecting-in-ruby)

#### Switch to master branch:
```
$ git checkout master
```

#### Deploy to Heroku app:
```
$ git push heroku local-branch-to-deploy:master
```

#### Check deploy output where `foo-app-94` is the name generated from heroku apps:create executed previously:
```
remote: -----> Launching...
remote:        Released v9
remote:        https://foo-app-94.herokuapp.com/ deployed to Heroku
```

#### Run DB migrations on Heroku app:
```
$ heroku run rails db:migrate
```

#### Now, open this link on your browser:
- [https://foo-app-94.herokuapp.com/](https://foo-app-94.herokuapp.com/)

#### Now, import the postman collection to your postman application from:
```
public/Ecommerce ViveTech.postman_collection.json
```
